'use strict';

const { IncomingWebhook } = require('@slack/webhook');
const BaseLogger = require('moleculer').Loggers.Base;

const util = require('util');

class SlackLogger extends BaseLogger {
    constructor(opts) {
        super(opts);

        const url = opts.slackToken || process.env.VMOL_LOGGER_SLACK_WEBHOOK_URL;

        if (url) {
            this.webhook = new IncomingWebhook(url);
        }

        this.defering = false;
        this.deferingTimeout = null;
        this.deferingDefaultRetryAfter = 1;
    }

    levelToIcon(level) {
        switch (level) {
            case 'fatal':
                return ':rotating_light:';

            case 'error':
                return ':no_entry:';

            case 'warn':
                return ':warning:';

            default:
                return ':white_check_mark:';
        }
    }

    getLogHandler(bindings) {
        const level = bindings ? this.getLogLevel(bindings.mod) : null;
        if (!this.webhook || !level) {
            return null;
        }

        const levelIdx = SlackLogger.LEVELS.indexOf(level);

        return (type, args) => {
            const typeIdx = SlackLogger.LEVELS.indexOf(type);
            if (this.defering || typeIdx > levelIdx) {
                return;
            }

            const message = {
                blocks: [{
                    type: 'section',
                    text: {
                        type: 'mrkdwn',
                        text: `*${bindings.ns}* ${this.levelToIcon(type)} ${type} _${bindings.nodeID}/${bindings.mod}_ ${args.shift()}`
                    }
                }]
            };

            for (const obj of args) {
                message.blocks.push({ type: 'divider' });
                message.blocks.push({
                    type: 'section',
                    text: {
                        type: 'mrkdwn',
                        text: '```' + util.inspect(obj) + '```'
                    }
                });
            }

            this.webhook.send(message)
                .catch(err => {
                    if (err?.original?.response?.status === 429) {
                        // if too many requests are sent, wait a while before sending
                        // anything to slack
                        const headers = err.original.response.headers || {};
                        return this.deferNextCall(headers['retry-after']);
                    }
                    throw err;
                });
        };
    }

    deferNextCall(waitSeconds) {
        this.defering = true;

        clearTimeout(this.deferingTimeout);

        this.deferingTimeout = setTimeout(() => {
            this.defering = false;
        }, 1000 * (waitSeconds || this.deferingDefaultRetryAfter));
    }
}

module.exports = SlackLogger;
